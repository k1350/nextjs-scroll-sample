import styles from "./page.module.css";

export default function ChildPage() {
  return (
    <section className={styles.ChildPage}>
      <h2>子ページの内容</h2>
    </section>
  );
}
