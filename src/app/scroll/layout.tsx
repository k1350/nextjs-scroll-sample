import styles from "./page.module.css";

type Props = {
  children: React.ReactNode;
};
export default function Layout({ children }: Props) {
  return (
    <section className={styles.ParentPage}>
      <h1 className={styles.ParentPageTitle}>親ページの内容</h1>
      {children}
    </section>
  );
}
